import React from 'react';
import { Provider } from 'react-redux';

import './App.scss';
import IndexPage from './components/pages/index/index';
import todosStore from './components/api/stores/todos-store';

const storeTodos = todosStore();

function App() {
  return (
    <Provider store={storeTodos}>
      <IndexPage />
    </Provider>
  );
}

export default App;
