import React from 'react';

import './index.scss';

const IndexTemplate:React.FC = () => {
  return (
    <section className="index-page">
      <div className="index-page--title-container">
        <div className="index-page--title"></div>
      </div>
      <section className="todos">
        <div className="todos-add">
          <div className="todos-add--input"></div>
          <div className="todos-add--save"></div>
        </div>
        <ul className="todos-list">
          <li className="todos-todo">
            <div className="todo-name"></div>
            <div className="todo-edit"></div>
            <div className="todo-complete"></div>
          </li>
          <li className="todos-todo">
            <div className="todo-name"></div>
            <div className="todo-edit"></div>
            <div className="todo-complete"></div>
          </li>
          <li className="todos-todo">
            <div className="todo-name"></div>
            <div className="todo-edit"></div>
            <div className="todo-complete"></div>
          </li>
        </ul>
      </section>
    </section>
  ); 
}

export default IndexTemplate;
