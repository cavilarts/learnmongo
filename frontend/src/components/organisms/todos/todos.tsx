import React, { ChangeEvent, useState, MouseEvent } from 'react';

import Input from '../../atoms/input/input';
import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import List from '../../molecules/list/list';
import { TodosProps } from './todos-interface';
import './todos.scss';

const Todos:React.FC<TodosProps> = (props) => {
  const { list, saveElement } = props;
  const changeInput = (e:ChangeEvent<HTMLInputElement>) => {
    setTodoValue(e.target.value);
  };
  const [todoValue, setTodoValue] = useState('');

  return (
    <>
      <div className="todos--add-todo">
        <Input type="text" name="add-todo" onChange={changeInput} placeholder="What needs to be done" />
        <Button name="add-todo" onClick={(event:React.MouseEvent<HTMLElement>) => { saveElement({todo:todoValue}) }}>
          <Icon name="plus" />
        </Button>
      </div>
      <List elements={list} editElement={saveElement}/>
    </>
  );
};

export default Todos;
