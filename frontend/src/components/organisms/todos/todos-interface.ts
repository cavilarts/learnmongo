import { ListElement } from '../../molecules/list/list-interface';

export interface TodosProps {
  list: ListElement[],
  saveElement: (todo:any) => void; 
}