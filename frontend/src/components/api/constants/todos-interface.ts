export interface RequestTodos {
  type: 'GET_TODOS';
}

export interface ReceiveTodos {
  type: 'RECEIVE_TODOS';
  todos: todoList
}

export interface ReceiveError {
  type: 'RECEIVE_ERROR';
}

export interface ReceiveTodo {
  type: 'RECEIVE_TODO';
}

export interface ReceiveUpdatedTodo {
  type: 'RECEIVE_UPDATED_TODO';
}

export interface ReceiveDeleteTodo {
  type: 'RECEIVE_DELETED_TODO';
}

export interface todoList {
  todos: todo[]
}

export interface todo {
  _id: string;
  todo: string;
}

export interface newTodo {
  todo: string;
  _id?:string;
  delete?:boolean;
}

export interface todosState {
  todosReducer: todosStateReducer;
}

export interface todosStateReducer {
  todos: todo[];
  error: boolean;
  loading: boolean;
}

export interface todoActions {
  type: RequestTodos['type'] | ReceiveTodos['type'] | ReceiveError['type'] | ReceiveTodo['type'] | ReceiveUpdatedTodo['type'] | ReceiveDeleteTodo['type'];
  todos?: todo[]
  todo: {
    document?: todo
    value: todo
  }
}