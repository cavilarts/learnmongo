import { GET_TODOS, RECEIVE_TODOS, RECEIVE_ERROR, RECEIVE_TODO, RECEIVE_UPDATED_TODO, RECEIVE_DELETED_TODO } from '../constants/action-types';
import { todosStateReducer, todoActions } from '../constants/todos-interface';

const initialState:todosStateReducer = {
  todos: [],
  error: false,
  loading: false
};

export default function todosReducer(state:todosStateReducer = initialState, action: todoActions):todosStateReducer {
  switch (action.type) {
    case GET_TODOS:
        return Object.assign({}, state, {
          loading: true
        });
    case RECEIVE_TODOS:
      return Object.assign({}, state, {
        todos: action.todos,
        loading: false
      });
    case RECEIVE_ERROR:
      return Object.assign({}, state, {
        error: true,
        loading: false
      });
    case RECEIVE_TODO:
      const todos = [...state.todos, action.todo.document]
      return Object.assign({}, state, {
        todos: todos,
        loading: false
      });
    case RECEIVE_UPDATED_TODO:
      const updatedTodos = state.todos.map(x => {
        if (x._id === action.todo.value._id) {
          x.todo = action.todo.value.todo;
        }
        return x;
      })
      return Object.assign({}, state, {
        todos: updatedTodos,
        loading: false
      });
    case RECEIVE_DELETED_TODO:
      const filteredTodos =  state.todos.filter(x => x._id !== action.todo.value._id);
      return Object.assign({}, state, {
        todos: filteredTodos,
        loading: false
      });
    default:
      return state;
  }
};