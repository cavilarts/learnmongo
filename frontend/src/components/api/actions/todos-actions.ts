import { GET_TODOS, RECEIVE_TODOS, RECEIVE_ERROR, RECEIVE_TODO, RECEIVE_UPDATED_TODO, RECEIVE_DELETED_TODO } from '../constants/action-types';
import { RequestTodos, ReceiveTodos, ReceiveError, todoList, todosState, newTodo } from '../constants/todos-interface';
import { Dispatch } from 'redux';

function requestTodos():RequestTodos {
  return {
    type: GET_TODOS
  };
}

function receiveTodos(response:todoList):ReceiveTodos {
  return {
    type: RECEIVE_TODOS,
    todos: response
  };
}

function receiveError():ReceiveError {
  return {
    type: RECEIVE_ERROR
  };
}

function receiveSavedTodo(response:any) {
  return {
    type: RECEIVE_TODO,
    todo: response
  };
}

function receiveUpdatedTodo(response:any) {
  return {
    type: RECEIVE_UPDATED_TODO,
    todo: response
  }
}

function receiveDeleteTodo(response:any) {
  return {
    type: RECEIVE_DELETED_TODO,
    todo: response
  }
}

function fetchTodos(): any {
  return (dispatch:Dispatch) => dispatchTodos(dispatch);
}

function dispatchTodos(dispatch:Dispatch):Promise<void | ReceiveTodos | ReceiveError> {
  dispatch(requestTodos());

  return fetch('http://localhost:3001/getTodos')
    .then(response => response.json())
    .then(response => dispatch(receiveTodos(response)))
    .catch(error => dispatch(receiveError()));
}

function shouldFetchTodos(state:todosState):boolean {
  return (!state.todosReducer.todos.length && !state.todosReducer.loading ) && !state.todosReducer.error;
}

function setTodo(todo:newTodo):any {
  return (dispatch:Dispatch) => {
    dispatch(requestTodos());

    return fetch('http://localhost:3001/', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(todo)
    })
    .then(response => response.json())
    .then(response => dispatch(receiveSavedTodo(response)))
    .catch(error => dispatch(receiveError()));
  }
}

function editTodo(todo:newTodo):any {
  return (dispatch:Dispatch) => {
    dispatch(requestTodos());
    
    return fetch(`http://localhost:3001/${todo._id}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({todo: todo.todo})
    })
    .then(response => response.json())
    .then(response => dispatch(receiveUpdatedTodo(response)))
    .catch(error => dispatch(receiveError()));
  }
}

function deleteTodo(todoId:string):any {
  return (dispatch:Dispatch) => {
    dispatch(requestTodos());

    return fetch(`http://localhost:3001/${todoId}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(response => dispatch(receiveDeleteTodo(response)))
    .catch(error => dispatch(receiveError()));
  }
}

export default function fetchTodosIfNeeded(todo?:newTodo): any {
  return (dispatch:Dispatch, getState:() => todosState) => {
    if (shouldFetchTodos(getState()) && !todo) {
      return dispatch(fetchTodos());
    } else if(todo?.todo && !todo?._id) {
      return dispatch(setTodo(todo));
    } else if (todo?._id && !todo.delete) {
      return dispatch(editTodo(todo));
    } else if (todo?._id && todo.delete) {
      return dispatch(deleteTodo(todo._id));
    }
  }
}

