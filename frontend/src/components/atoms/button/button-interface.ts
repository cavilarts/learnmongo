import { ReactNode, MouseEvent } from "react";

export interface ButtonProps {
  chilren?: ReactNode;
  name: string;
  type?: 'submit' | 'reset' | 'button';
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}