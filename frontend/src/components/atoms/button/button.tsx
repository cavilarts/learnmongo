import React from 'react';

import { ButtonProps } from './button-interface';
import './button.scss';

const Button:React.FC<ButtonProps> = (props) => {
  const { children, name, type, onClick } = props;

  return <button name={name} type={type} onClick={onClick} className="button">{children}</button>
};

export default Button;
