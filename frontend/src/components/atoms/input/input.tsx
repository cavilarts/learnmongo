import React from 'react';

import { InputProps } from './input-interface';
import './input.scss';

const Input:React.FC<InputProps> = (props) => {
  const {
    type,
    name,
    disabled,
    checked,
    value,
    onClick,
    children,
    onChange,
    placeholder
  } = props;

  return <input
    type={type}
    name={name}
    disabled={disabled}
    checked={checked}
    autoFocus={true}
    value={value}
    onClick={onClick}
    onChange={onChange}
    className={`input`}
    placeholder={placeholder}
  >
    { children ? children : null }
  </input>
};

export default Input;