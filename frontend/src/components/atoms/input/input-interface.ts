import { ReactNode, ChangeEvent } from "react";

export interface InputProps {
  type: 'button' | 'checkbox' | 'radio' | 'text' | 'submit';
  name: string;
  disabled?: boolean;
  checked?: boolean;
  autofocus?: boolean;
  value?: string | number;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  children?: ReactNode;
  placeholder?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void; 
} 