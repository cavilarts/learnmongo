import React from 'react';

import { IconProps } from './icon-interface';
import './icon.scss';

const Icon:React.FC<IconProps> = (props) => {
  const { name } = props;
  const className = `icon-${name}`;
  return <div className={className}></div>
}

export default Icon;
