export interface IconProps {
  name: 'pencil2' | 'floppy-disk' | 'spinner9' | 'bin2' | 'plus' | 'checkbox-checked' | 'checkbox-unchecked' | 'more-horizontal';
};