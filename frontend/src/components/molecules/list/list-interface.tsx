export interface ListProps {
  elements: ListElement[]
  editElement: (todo:any) => void;
}

export interface ListElement {
  _id: string;
  todo: string;
  cb?: (todo:any) => void;
}

export interface ListElementWithCB {
  _id: string;
  todo: string;
  cb: (todo:any) => void;
}