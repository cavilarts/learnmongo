import React, { ReactElement, MouseEvent, useState, ChangeEvent } from 'react';

import Icon from '../../atoms/icon/icon';
import Button from '../../atoms/button/button';
import Input from '../../atoms/input/input';
import { ListProps, ListElementWithCB } from './list-interface';
import './list.scss';

const List:React.FC<ListProps> = (props) => {
  const { elements, editElement } = props;
  const elemetsWithCallback:ListElementWithCB[] = elements.map((e, i) => {
    return Object.assign({}, e, {cb: editElement});
  })

  return (
    <ul className="list">
      {elemetsWithCallback.map(RenderElement)}
    </ul>
  );
};

function RenderElement(el:ListElementWithCB, i:number):ReactElement {
  const [inputValue, setInputValue] = useState(el.todo);
  const [disabled, setDisabled] = useState(true);
  const [moreStatus, setMoreStatus] = useState('closed');

  return (
    <li key={i}>
      <div>
        <Input 
          type="text"
          value={inputValue}
          name="todo"
          onChange={(e:ChangeEvent<HTMLInputElement>) => {
            setInputValue(e.target.value);
          }}
          disabled={disabled}
          autofocus={disabled}
        />
        <Button name="check-uncheck" onClick={(event:MouseEvent) => {}}>
          <Icon name="checkbox-unchecked"  />
        </Button>
        <Button name="more" onClick={(event:MouseEvent) => {
          moreStatus === 'open' && !disabled ? setDisabled(true) : setDisabled(false);
          setMoreStatus(moreStatus === 'closed' ? 'open': 'closed')
           
        }}>
          <Icon name="more-horizontal"  />
        </Button>
    </div>
    <div className={`list-controls-more ${moreStatus}`}>
      <Button name="delete" onClick={(event:MouseEvent) => {
        el.cb({
          _id: el._id,
          delete: true
        });
      }}>
        <Icon name="bin2"  />
      </Button>
      <Button name="save" onClick={(event:MouseEvent) => {
          el.cb({
            _id: el._id,
            todo: inputValue
          });
        }
      }>
        <Icon name="floppy-disk"  />
      </Button>
    </div>
    </li>
  );
};

export default List;