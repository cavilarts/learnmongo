import React, { ReactElement } from 'react';
import { connect } from 'react-redux';

import { IndexProps } from './index-interface';
import './index.scss';
import Todos from '../../organisms/todos/todos';
import { todo, todosState } from '../../api/constants/todos-interface';
import fetchTodosIfNeeded from '../../api/actions/todos-actions';

const mapState = (state:todosState) => {
  return ({
    todos: state.todosReducer.todos,
    loading: state.todosReducer.loading,
    error: state.todosReducer.error
  })
};

const IndexPage:React.FC<IndexProps> = (props) => {
  const {todos, loading, error, fetchTodosIfNeeded } = props;

  fetchTodosIfNeeded();

  return (
    <section className="index-page">
      {loading ? renderLoading() : renderContent(error, todos, fetchTodosIfNeeded)}
    </section>

  );
};

function renderLoading() {
  return <section>Loading...</section>
}

function renderContent (error:boolean, todos:todo[], fetchTodosIfNeeded:any) {
    return (
      <>
        {error ? renderError() : renderTodos(todos, fetchTodosIfNeeded)}
      </>
    )
}

function renderError():ReactElement {
  return <p>Cannot load the todos</p>
}

function renderTodos(todos:todo[], fetchTodosIfNeeded:any):ReactElement {
  return (
    <Todos list={todos} saveElement={fetchTodosIfNeeded} />
  );
}

const mapDispatchToProps = { fetchTodosIfNeeded };

export default connect(mapState, mapDispatchToProps)(IndexPage);
