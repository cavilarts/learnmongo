import { todo } from '../../api/constants/todos-interface';

export interface IndexProps {
  todos: todo[],
  loading: boolean;
  error: boolean;
  fetchTodosIfNeeded: () => void;
}
