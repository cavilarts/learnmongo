const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const Joi = require('joi');

const db = require('./db');

const app = express();
const collection = 'todo';
const port = 3001;
const parser = bodyParser.urlencoded({ extended: false });

const shcema = Joi.object().keys({
  todo: Joi.string().required()
});

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send(path.join(__dirname, 'index.html'));
});

app.get('/getTodos', (req, res) => {
  db.getDB().collection(collection).find({}).toArray((err, documents) => {
    if(err) {
      console.log(err);
    } else {
      console.log(documents);
      res.json(documents);
    }
  });
});

app.put('/:id', parser, (req, res) => {
  const todoId = req.params.id;
  const todoInput = req.body;

  db
    .getDB()
    .collection(collection)
    .findOneAndUpdate(
      { _id: db.getPrimaryKey(todoId) },
      { $set: { todo: todoInput.todo } },
      { returnOriginal: false },
      (err, response) => {
        if (err) {
          console.log(err);
        } else {
          res.json(response);
        }
      }
    );
});

app.post('/', (req, res) => {
  const todo = req.body;

  Joi.validate(todo, shcema, (err, result) => {
    if (err) {
      const error = new Error('Invalid Input');

      error.status = 400;
      next(error);
    } else {
      db
        .getDB()
        .collection(collection)
        .insertOne(todo, (err, result) => {
          if (err) {
            const error = new Error('Failed to create a todo');

            error.status = 400;
            next(error);
          } else {
            res.json({result: result, document: result.ops[0], mng: "Todo created succesfully"});
          }
        });
    }
  });
});

app.delete('/:id', (req, res) => {
  const todoID = req.params.id;

  db
    .getDB()
    .collection(collection)
    .findOneAndDelete({
      _id: db.getPrimaryKey(todoID)
    }, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.json(result);
      }
    });
});

app.use((err, req, res, next) => {
  res.status(err.status).json({
    error: {
      message: err.message
    }
  });
});

db.connect((err) => {
  if (err) {
    console.log('Unable to connect to database', err);
    process.exit(1);
  } else {
    app.listen(port, () => {
      console.log(`Connected to DB, listeing on port ${port}`);
    });
  }
});
